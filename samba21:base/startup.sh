#! /bin/bash

mkdir /var/lib/samba/public && chmod 777 /var/lib/samba/public
#cp /usr/bin/cal /usr/bin/date /var/lib/samba/public
#date > /var/lib/samba/public/date.txt
uname -a > /var/lib/samba/public/uname.txt

cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/nslcd.conf /etc/nslcd.conf

/usr/sbin/nscd
/usr/sbin/nslcd

bash /opt/docker/users_ldap.sh

cp smb.alone.conf /etc/samba/smb.conf
#mkdir /run/smbd
/usr/sbin/smbd
/usr/sbin/nmbd -F
######################################################
llistaUsers="pere marta anna pau pere jordi"
for user in $llistaUsers
do
  echo -e "$user\n$user" | smbpasswd -a $user
  line=$(getent passwd $user)
  uid=$(echo $line | cut -d: -f3)
  gid=$(echo $line | cut -d: -f4)
  homedir=$(echo $line | cut -d: -f6)
  echo "$user $uid $gid $homedir"
  if [ ! -d $homedir ]; then
    mkdir -p $homedir
    cp -ra /etc/skel/. $homedir
    chown -R $uid.$gid $homedir
  fi
done
